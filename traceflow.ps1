Function New-DynamicParam {
<#
    .SYNOPSIS
        Helper function to simplify creating dynamic parameters
    
    .DESCRIPTION
        Helper function to simplify creating dynamic parameters

        Example use cases:
            Include parameters only if your environment dictates it
            Include parameters depending on the value of a user-specified parameter
            Provide tab completion and intellisense for parameters, depending on the environment

        Please keep in mind that all dynamic parameters you create will not have corresponding variables created.
           One of the examples illustrates a generic method for populating appropriate variables from dynamic parameters
           Alternatively, manually reference $PSBoundParameters for the dynamic parameter value

    .NOTES
        Credit to http://jrich523.wordpress.com/2013/05/30/powershell-simple-way-to-add-dynamic-parameters-to-advanced-function/
            Added logic to make option set optional
            Added logic to add RuntimeDefinedParameter to existing DPDictionary
            Added a little comment based help

        Credit to BM for alias and type parameters and their handling

    .PARAMETER Name
        Name of the dynamic parameter

    .PARAMETER Type
        Type for the dynamic parameter.  Default is string

    .PARAMETER Alias
        If specified, one or more aliases to assign to the dynamic parameter

    .PARAMETER ValidateSet
        If specified, set the ValidateSet attribute of this dynamic parameter

    .PARAMETER Mandatory
        If specified, set the Mandatory attribute for this dynamic parameter

    .PARAMETER ParameterSetName
        If specified, set the ParameterSet attribute for this dynamic parameter

    .PARAMETER Position
        If specified, set the Position attribute for this dynamic parameter

    .PARAMETER ValueFromPipelineByPropertyName
        If specified, set the ValueFromPipelineByPropertyName attribute for this dynamic parameter

    .PARAMETER HelpMessage
        If specified, set the HelpMessage for this dynamic parameter
    
    .PARAMETER DPDictionary
        If specified, add resulting RuntimeDefinedParameter to an existing RuntimeDefinedParameterDictionary (appropriate for multiple dynamic parameters)
        If not specified, create and return a RuntimeDefinedParameterDictionary (appropriate for a single dynamic parameter)

        See final example for illustration

    .EXAMPLE
        
        function Show-Free
        {
            [CmdletBinding()]
            Param()
            DynamicParam {
                $options = @( gwmi win32_volume | %{$_.driveletter} | sort )
                New-DynamicParam -Name Drive -ValidateSet $options -Position 0 -Mandatory
            }
            begin{
                #have to manually populate
                $drive = $PSBoundParameters.drive
            }
            process{
                $vol = gwmi win32_volume -Filter "driveletter='$drive'"
                "{0:N2}% free on {1}" -f ($vol.Capacity / $vol.FreeSpace),$drive
            }
        } #Show-Free

        Show-Free -Drive <tab>

    # This example illustrates the use of New-DynamicParam to create a single dynamic parameter
    # The Drive parameter ValidateSet populates with all available volumes on the computer for handy tab completion / intellisense

    .EXAMPLE

    # I found many cases where I needed to add more than one dynamic parameter
    # The DPDictionary parameter lets you specify an existing dictionary
    # The block of code in the Begin block loops through bound parameters and defines variables if they don't exist

        Function Test-DynPar{
            [cmdletbinding()]
            param(
                [string[]]$x = $Null
            )
            DynamicParam
            {
                #Create the RuntimeDefinedParameterDictionary
                $Dictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        
                New-DynamicParam -Name AlwaysParam -ValidateSet @( gwmi win32_volume | %{$_.driveletter} | sort ) -DPDictionary $Dictionary

                #Add dynamic parameters to $dictionary
                if($x -eq 1)
                {
                    New-DynamicParam -Name X1Param1 -ValidateSet 1,2 -mandatory -DPDictionary $Dictionary
                    New-DynamicParam -Name X1Param2 -DPDictionary $Dictionary
                    New-DynamicParam -Name X3Param3 -DPDictionary $Dictionary -Type DateTime
                }
                else
                {
                    New-DynamicParam -Name OtherParam1 -Mandatory -DPDictionary $Dictionary
                    New-DynamicParam -Name OtherParam2 -DPDictionary $Dictionary
                    New-DynamicParam -Name OtherParam3 -DPDictionary $Dictionary -Type DateTime
                }
        
                #return RuntimeDefinedParameterDictionary
                $Dictionary
            }
            Begin
            {
                #This standard block of code loops through bound parameters...
                #If no corresponding variable exists, one is created
                    #Get common parameters, pick out bound parameters not in that set
                    Function _temp { [cmdletbinding()] param() }
                    $BoundKeys = $PSBoundParameters.keys | Where-Object { (get-command _temp | select -ExpandProperty parameters).Keys -notcontains $_}
                    foreach($param in $BoundKeys)
                    {
                        if (-not ( Get-Variable -name $param -scope 0 -ErrorAction SilentlyContinue ) )
                        {
                            New-Variable -Name $Param -Value $PSBoundParameters.$param
                            Write-Verbose "Adding variable for dynamic parameter '$param' with value '$($PSBoundParameters.$param)'"
                        }
                    }

                #Appropriate variables should now be defined and accessible
                    Get-Variable -scope 0
            }
        }

    # This example illustrates the creation of many dynamic parameters using New-DynamicParam
        # You must create a RuntimeDefinedParameterDictionary object ($dictionary here)
        # To each New-DynamicParam call, add the -DPDictionary parameter pointing to this RuntimeDefinedParameterDictionary
        # At the end of the DynamicParam block, return the RuntimeDefinedParameterDictionary
        # Initialize all bound parameters using the provided block or similar code

    .FUNCTIONALITY
        PowerShell Language

#>
param(
    
    [string]
    $Name,
    
    [System.Type]
    $Type = [string],

    [string[]]
    $Alias = @(),

    [string[]]
    $ValidateSet,
    
    [switch]
    $Mandatory,
    
    [string]
    $ParameterSetName="__AllParameterSets",
    
    [int]
    $Position,
    
    [switch]
    $ValueFromPipelineByPropertyName,
    
    [string]
    $HelpMessage,

    [validatescript({
        if(-not ( $_ -is [System.Management.Automation.RuntimeDefinedParameterDictionary] -or -not $_) )
        {
            Throw "DPDictionary must be a System.Management.Automation.RuntimeDefinedParameterDictionary object, or not exist"
        }
        $True
    })]
    $DPDictionary = $false
 
)
    #Create attribute object, add attributes, add to collection   
        $ParamAttr = New-Object System.Management.Automation.ParameterAttribute
        $ParamAttr.ParameterSetName = $ParameterSetName
        if($mandatory)
        {
            $ParamAttr.Mandatory = $True
        }
        if($Position -ne $null)
        {
            $ParamAttr.Position=$Position
        }
        if($ValueFromPipelineByPropertyName)
        {
            $ParamAttr.ValueFromPipelineByPropertyName = $True
        }
        if($HelpMessage)
        {
            $ParamAttr.HelpMessage = $HelpMessage
        }
 
        $AttributeCollection = New-Object 'Collections.ObjectModel.Collection[System.Attribute]'
        $AttributeCollection.Add($ParamAttr)
    
    #param validation set if specified
        if($ValidateSet)
        {
            $ParamOptions = New-Object System.Management.Automation.ValidateSetAttribute -ArgumentList $ValidateSet
            $AttributeCollection.Add($ParamOptions)
        }

    #Aliases if specified
        if($Alias.count -gt 0) {
            $ParamAlias = New-Object System.Management.Automation.AliasAttribute -ArgumentList $Alias
            $AttributeCollection.Add($ParamAlias)
        }

 
    #Create the dynamic parameter
        $Parameter = New-Object -TypeName System.Management.Automation.RuntimeDefinedParameter -ArgumentList @($Name, $Type, $AttributeCollection)
    
    #Add the dynamic parameter to an existing dynamic parameter dictionary, or create the dictionary and add it
        if($DPDictionary)
        {
            $DPDictionary.Add($Name, $Parameter)
        }
        else
        {
            $Dictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
            $Dictionary.Add($Name, $Parameter)
            $Dictionary
        }
}





function Start-NSXTraceflow {

    <#
    .SYNOPSIS
    Starts a NSX traceflow

    .DESCRIPTION
    A NSX traceflow tests and validates the flow of traffic between VXLAN-backed virtual nics and an arbitary endpoint such as another vNic, 
    an IP address, a MAC addres, a L2 multicast or l2-broadcast

    This cmdlet returns a traceflow ID which can be consumed by get-nsxtraceflow

    .EXAMPLE
    PS C:\> start-nsxtraceflow -sourcevNic (get-vm "vm1" | get-networkadapter) -Protocol tcp -TrafficType l2-unicast -DestinationIpAddress 1.2.3.4

    #>



    [CmdLetBinding(DefaultParameterSetName="DestinationIpAddress")]


    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
            #Network Adapter or collection to use as source VM for the traceflow
            [ValidateNotNullOrEmpty()]
            [VMware.VimAutomation.ViCore.Interop.V1.VirtualDevice.NetworkAdapterInterop[]]$SourcevNic,
        [Parameter (Mandatory=$true)]
            [ValidateSet("udp","tcp","icmp",IgnoreCase=$true)]
            [string]$Protocol,
        [Parameter (Mandatory=$true)]
            [ValidateSet("l2-unicast","l3-unicast","l2-multicast","l2-broadcast",IgnoreCase=$true)]
            [string]$TrafficType,
        [Parameter (Mandatory=$false)]
            [ValidateRange(5000,15000)]
            [int]$Timeout = 10000
    )

    DynamicParam {
        $Dictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
        switch ($Protocol) {
            "icmp" {
                #9999 is completely arbitary
                new-dynamicparam -name ICMPID -type int -DPDictionary $Dictionary -ValidateSet (1..9999) 
                new-dynamicparam -name SequenceID -type int -DPDictionary $Dictionary -ValidateSet (1..9999) 
                new-dynamicparam -name TTL -type int  -DPDictionary $Dictionary -ValidateSet (1..64)

            }
            "tcp" {

                # Set TCP flag values so we can refer to them later
                $TCPFlagsBinaryValues = @{
                    "FYN" = [int]1;
                    "SYN" = [int]2;
                    "RST" = [int]4;
                    "PSH" = [int]8;
                    "ACK" = [int]16;
                    "URG" = [int]32;
                    "ECE" = [int]64;
                    "CWR" = [int]128;
                }

                new-dynamicparam -name SourcePort -type int -Mandatory -DPDictionary $Dictionary -ValidateSet (1..65535)
                new-dynamicparam -name DestinationPort -type int -Mandatory -DPDictionary $Dictionary -ValidateSet (1..65535)
                new-dynamicparam -name TCPFlags -type string[] -DPDictionary $Dictionary -ValidateSet ($TCPFlagsBinaryValues.Keys)
            }
            "udp" {
                new-dynamicparam -name SourcePort -type int -Mandatory -DPDictionary $Dictionary -ValidateSet (1..65535)
                new-dynamicparam -name DestinationPort -type int -Mandatory -DPDictionary $Dictionary -ValidateSet (1..65535)
            }

        }
        
        switch ($TrafficType) {
            "l2-unicast" {
                new-dynamicparam -name DestinationMacAddress -type string -Mandatory -DPDictionary $Dictionary -ParameterSetName "DestinationMacAddress"
                new-dynamicparam -name DestinationIpAddress -type string -Mandatory -DPDictionary $Dictionary -ParameterSetName "DestinationIpAddress"
                new-dynamicparam -name DestinationvNic -type VMware.VimAutomation.ViCore.Interop.V1.VirtualDevice.NetworkAdapterInterop[] -Mandatory -DPDictionary $Dictionary -ParameterSetName "DestinationVnic"
            }
            
            "l3-unicast" {
                new-dynamicparam -name DestinationIpAddress -type string -Mandatory -DPDictionary $Dictionary -ParameterSetName "DestinationIpAddress"
                new-dynamicparam -name DestinationvNic -type VMware.VimAutomation.ViCore.Interop.V1.VirtualDevice.NetworkAdapterInterop[] -Mandatory -DPDictionary $Dictionary -ParameterSetName "DestinationVnic"
            }

            "l2-multicast" {
                new-dynamicparam -name DestinationIpAddress -type string -Mandatory -DPDictionary $Dictionary -ParameterSetName "DestinationIpAddress"
            }

            "l2-broadcast" {
                new-dynamicparam -name Subnetprefix -type int -Mandatory -DPDictionary $Dictionary -ValidateSet (0..32)
            }

        }
    $Dictionary
    } 




    begin {
        # Set function dynamic parameters
        $PSBoundParameters.Keys |% {
            if (-not ( Get-Variable -name $_ -scope 0 -ErrorAction SilentlyContinue ) ) {
                New-Variable -Name $_ -Value $PSBoundParameters.$_
                Write-Verbose "Adding variable for dynamic parameter ‘$_ with value ‘$($PSBoundParameters.$_)'"
            }
        }
    
#          if ( $Connection.Version -le 6.2) {
#                  throw "Traceflow requires NSX 6.2 or higher, quitting.. $_"
#          } 

        # Set Function statics
        $EthType = 2048

        # Set function dynamics

        #Process source vNic
        $vmUuid = ($SourcevNic.parent | get-view).config.instanceuuid
        $vnicUuid = "$vmUuid.$($SourcevNic.id.substring($SourcevNic.id.length-3))"
        $SourceMacAddress = $SourcevNic.MacAddress
        $SourceIpAddress =  $SourcevNic.Parent.ExtensionData.Guest.IpAddress


        if ($TCPFlags -eq $null ) {
            Set-Variable "TCPFLAGS" -Value @("SYN")
        }

        



    }

    process {

        #Add XML root objects and objects always present
        [System.XML.XMLDocument]$xmlDoc = New-Object System.XML.XMLDocument
        [System.XML.XMLElement]$xmlRoot = $XMLDoc.CreateElement("traceflowRequest")
        Add-XmlElement -xmlRoot $xmlRoot -xmlElementName "vnicId" -xmlElementText $vnicUuid
        Add-XmlElement -xmlRoot $xmlRoot -xmlElementName "timeout" -xmlElementText $timeout

        #Set routed flag based on traffictype
        if ( $TrafficType -like "l2-*" ) {
            Add-XmlElement -xmlRoot $xmlRoot -xmlElementName "routed" -xmlElementText "false"
        }

        elseIf ( $TrafficType -like "l3-*" ) {
            Add-XmlElement -xmlRoot $xmlRoot -xmlElementName "routed" -xmlElementText "true"
        }
     

        # Create packet elements , always present in request 
 
        [System.XML.XMLElement]$XMLPacketElement = $XMLDoc.CreateElement("packet")
        [System.XML.XMLAttribute]$XMLPacketAttribute = $XMLDoc.CreateAttribute("class")
        $XMLPacketAttribute.Value = "fieldsPacketData"
        $XMLPacketElement.Attributes.Append($XMLPacketAttribute) | Out-Null
        Add-XmlElement -xmlRoot $XMLPacketElement -xmlElementName "resourceType" -xmlElementText "FieldsPacketData"

        # Create ethheader elements, always present in request
        [System.XML.XMLElement]$XMLEthHeaderElement = $XMLDoc.CreateElement("ethHeader")
        Add-XmlElement -xmlRoot $XMLEthHeaderElement -xmlElementName "srcMac" -xmlElementText $SourceMacAddress

        # Either DestinationMacAddress or $DestinationIPAddress or $DestinationvNic must be provided but both are used in the request
        if ($DestinationMacAddress) {
            set-variable -name "DestinationIPAddress" -value "0.0.0.0"
        }
        elseif ($DestinationvNic) {
            set-variable -name "DestinationMacAddress" -value $DestinationvNic.MacAddress
            set-variable -name "DestinationIPAddress" -value $DestinationvNic.Parent.ExtensionData.Guest.IpAddress            
        }
        elseif ($DestinationIPAddress) {
            Set-Variable -name "DestinationMacAddress" -Value "00:00:00:00:00:00"
        }
        
        Add-XmlElement -xmlRoot $XMLEthHeaderElement -xmlElementName "dstMac" -xmlElementText $DestinationMacAddress
        Add-XmlElement -xmlRoot $XMLEthHeaderElement -xmlElementName "ethType" -xmlElementText "2048"

        $XMLPacketElement.AppendChild($XMLEthHeaderElement) | Out-Null 

        # Create ipHeader elements, always present in request 

        [System.XML.XMLElement]$XMLIpHeaderElement = $XMLDoc.CreateElement("ipHeader")
        Add-XmlElement -xmlRoot $XMLIpHeaderElement -xmlElementName "srcIp" -xmlElementText $SourceIpAddress



        Add-XmlElement -xmlRoot $XMLIpHeaderElement -xmlElementName "dstIp" -xmlElementText $DestinationIPAddress

        if (($Protocol -eq "icmp") -and ($TTL))  {
            Add-XmlElement -xmlRoot $XMLIpHeaderElement -xmlElementName "ttl" -xmlElementText $TTL 

        }
        $XMLPacketElement.AppendChild($XMLIpHeaderElement) | Out-Null 


        # Create transportheader elements, always present in request



        switch ($Protocol) {
            "icmp" {

                if ( ($SequenceID) -or ($ICMPID) ) {
                    [System.XML.XMLElement]$XMLTransportHeaderElement = $XMLDoc.CreateElement("transportHeader")
                    [System.XML.XMLElement]$XMLICMPEchoRequestHeaderElement = $XMLDoc.CreateElement("icmpEchoRequestHeader")
                    Add-XmlElement -xmlRoot $XMLICMPEchoRequestHeaderElement -xmlElementName "sequence" -xmlElementText $SequenceID
                    Add-XmlElement -xmlRoot $XMLICMPEchoRequestHeaderElement -xmlElementName "id" -xmlElementText $ICMPID
                    $XMLTransportHeaderElement.AppendChild($XMLICMPEchoRequestHeaderElement) | Out-Null
                    $XMLPacketElement.AppendChild($XMLTransportHeaderElement) | Out-Null
                    $xmlroot.AppendChild($XMLPacketElement) | out-null
                    $xmlDoc.AppendChild($xmlRoot) | out-null
                } 
            }

            "tcp" {
                $TCPFlagsBinaryValue = 0
                $TCPFlags |% {$TCPFlagsBinaryValue += $TCPFlagsBinaryValues.Item($_)}

                [System.XML.XMLElement]$XMLTransportHeaderElement = $XMLDoc.CreateElement("transportHeader")
                [System.XML.XMLElement]$TCPHeaderElement = $XMLDoc.CreateElement("tcpHeader")  
                Add-XmlElement -xmlRoot $TCPHeaderElement -xmlElementName "srcPort" -xmlElementText $SourcePort
                Add-XmlElement -xmlRoot $TCPHeaderElement -xmlElementName "dstPort" -xmlElementText $DestinationPort
                Add-XmlElement -xmlRoot $TCPHeaderElement -xmlElementName "tcpFlags" -xmlElementText $TCPFlagsBinaryValue
                $XMLTransportHeaderElement.AppendChild($TCPHeaderElement) | Out-Null
                $XMLPacketElement.AppendChild($XMLTransportHeaderElement) | Out-Null
                $xmlroot.AppendChild($XMLPacketElement) | out-null
                $xmlDoc.AppendChild($xmlRoot) | out-null
            }

            "udp" {

                [System.XML.XMLElement]$XMLTransportHeaderElement = $XMLDoc.CreateElement("transportHeader")
                [System.XML.XMLElement]$UDPHeaderElement = $XMLDoc.CreateElement("udpHeader")  
                Add-XmlElement -xmlRoot $UDPHeaderElement -xmlElementName "srcPort" -xmlElementText $SourcePort
                Add-XmlElement -xmlRoot $UDPHeaderElement -xmlElementName "dstPort" -xmlElementText $DestinationPort
                $XMLTransportHeaderElement.AppendChild($UDPHeaderElement) | Out-Null
                $XMLPacketElement.AppendChild($XMLTransportHeaderElement) | Out-Null
                $xmlroot.AppendChild($XMLPacketElement) | out-null
                $xmlDoc.AppendChild($xmlRoot) | out-null
            }
        }

        #Do the post
        $body = $xmlroot.OuterXml
        write-host $body
        $URI = "/api/2.0/vdn/traceflow"
        $response = invoke-nsxwebrequest -method "post" -uri $URI -body $body -connection $connection

        if ( $response.content ) {
            $return = new-object psobject
            $return | add-member -memberType NoteProperty -Name "TraceFlowId" -value $response.content
            return $return
        }
        else {
            throw "Error submitting traceflow"
        }
    }


    end {}
} 


function Get-NSXTraceflowResult {

    <#
    .SYNOPSIS
    Retrieve the result of a NSX traceflow

    .DESCRIPTION
    A NSX traceflow tests and validates the flow of traffic between VXLAN-backed virtual nics and an arbitary endpoint such as another vNic, 
    an IP address, a MAC addres, a L2 multicast or l2-broadcast. This cmdlet retrieves the status and overview of the traceflow process

    This cmdlet returns a traceflow object 

    .EXAMPLE
    PS C:\> get-nsxtraceflowResult -TraceFlowId 00000000-0000-0000-0000-00000e3ebc30 
    #>

    [CmdLetBinding()]

   param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
            #Network Adapter or collection to use as source VM for the traceflow
            [ValidateNotNullOrEmpty()]
            [string]$TraceFlowId
    )



    begin {
  #      if ( $Connection.Version -le 6.2) {
  #              throw "Traceflow requires NSX 6.2 or higher, quitting.. $_"
  #      }
    }

    process {


        #Do the post

        $URI = "/api/2.0/vdn/traceflow/$TraceFlowId"
        $response = invoke-nsxwebrequest -method "get" -uri $URI -connection $connection
        $return = ([xml]$response.content).traceflowDto
        return $return


    } 

    end {}
} 


function Get-NSXTraceflowObservations {

    <#
    .SYNOPSIS
    Retrieve the obervations of a NSX traceflow 

    .DESCRIPTION
    A NSX traceflow tests and validates the flow of traffic between VXLAN-backed virtual nics and an arbitary endpoint such as another vNic, 
    an IP address, a MAC addres, a L2 multicast or l2-broadcast. This cmdlet retrieves the status and overview of the traceflow process

    This cmdlet returns a traceflow observation object 

    .EXAMPLE
    PS C:\> get-nsxtraceflowObservations -TraceFlowId 00000000-0000-0000-0000-00000e3ebc30 
    #>

    [CmdLetBinding()]

   param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
            #Network Adapter or collection to use as source VM for the traceflow
            [ValidateNotNullOrEmpty()]
            [string]$TraceFlowId
    )



    begin {
  #      if ( $Connection.Version -le 6.2) {
  #              throw "Traceflow requires NSX 6.2 or higher, quitting.. $_"
  #      }
    }

    process {


        #Do the post

        $URI = "/api/2.0/vdn/traceflow/$TraceFlowId/observations"
        $response = invoke-nsxwebrequest -method "get" -uri $URI -connection $connection
        $return = ([xml]$response.content).traceflowObservations.traceflowObservationsDataPage
        return $return


    } 

    end {}
} 







